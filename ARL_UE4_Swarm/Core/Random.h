#pragma once

// Includes
#include <stdlib.h>
#include <random>
#include <chrono>


// Defines
#ifdef _DEBUG
#define RANDOM_NO_SEED
#endif




#define Randomizer Random::GetInstance()

// Class Random
class Random
{
public:
	static Random& GetInstance()
	{
		if (s_instance == NULL) { s_instance = new Random(); }
		return *s_instance;
	}

	float GetFloat(float min = 0.0f, float max = 1.0f)
	{
		std::uniform_real<float> dis(min, max);
		return dis(mersenneTwister);
	}

	int GetInt(int min = 0, int max = 100)
	{
		std::uniform_int<int> dis(min, max);
		return dis(mersenneTwister);
	}

private:
	Random()
	{
#ifdef RANDOM_NO_SEED

#else
		unsigned int seed1 = (unsigned int)std::chrono::system_clock::now().time_since_epoch().count();
		mersenneTwister.seed(seed1);
#endif
	}
	std::mt19937 mersenneTwister;

	// Statics
	static Random* s_instance;
};


