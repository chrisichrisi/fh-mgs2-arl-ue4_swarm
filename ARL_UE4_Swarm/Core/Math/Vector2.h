
#pragma once
template <class T>
class Vector2
{
	//--------------------------------------------------------------------------------------
	// Members
	//--------------------------------------------------------------------------------------
public:
	T X;
	T Y;



public:
	Vector2(T x = 0, T y = 0) :
		X(x), Y(y)
	{
	}



	Vector2& Add(const Vector2<T>& v)		{ X += v.X;		Y += v.Y;	return *this; }
	Vector2& Add(T x, T y)					{ X += x;		Y += y;		return *this; }
	Vector2& Subtract(const Vector2<T>& v)	{ X -= v.X;		Y -= v.Y;	return *this; }
	Vector2& Subtract(T x, T y)				{ X -= x;		Y -= y;		return *this; }
	Vector2& Scale(const Vector2& v)		{ X *= v.X;		Y *= v.Y;	return *this; }
	Vector2& Scale(T x, T y)				{ X *= x;		Y *= y;		return *this; }
	Vector2& Scale(T scale)					{ X *= scale;	Y *= scale;	return *this; }
	Vector2& Divide(const Vector2& v)		{ X /= v.X;		Y /= v.Y;	return *this; }
	Vector2& Divide(T x, T y)				{ X /= x;		Y /= y;		return *this; }
	Vector2& Divide(T divid)				{ X /= divid;	Y /= divid;	return *this; }



	void Set(T x, T y) { X = x; Y = y; }





	float SqrMagnitude() const	{ return Vector2::Dot(*this, *this); }
	float Magnitude() const		{ return sqrt(SqrMagnitude()); }
	Vector2 Normalized() const	
	{ 
		if (X == 0.0f && Y == 0.0f) { return *this; }
		float m = Magnitude(); return Vector2(X / m, Y / m); 
	}




	void RotateByRAD(float rads)
	{
		float tempX = X, tempY = Y;
		X = tempX * cos(rads) - tempY * sin(rads);
		Y = tempY * cos(rads) - tempX * sin(rads);
	}




	static float Vector2::Dot(const Vector2& lhs, const Vector2& rhs) { return static_cast<float>((lhs.X * rhs.X) + (lhs.Y * rhs.Y)); }
	static float Vector2::Distance(const Vector2& p1, const Vector2& p2) { return Vector2(p2.X - p1.X, p2.Y - p1.Y).Magnitude(); }

	// Operator overloading



	bool Vector2::Equal(const Vector2& v1, const Vector2 &v2)
	{
		if (&v1 == &v2) { return true; }
		if (v1.X != v2.X) { return false; }
		if (v1.Y != v2.Y) { return false; }
		return true;
	}


	static Vector2 SPlus(const Vector2& v1, const Vector2& v2)		{ return Vector2(v1.X + v2.X, v1.Y + v2.Y); }
	static Vector2 SPlus(const Vector2& v1, T plus)				{ return Vector2(v1.X + plus, v1.Y + plus); }
	static Vector2 SMinus(const Vector2& v1, const Vector2& v2)	{ return Vector2(v1.X - v2.X, v1.Y - v2.Y); }
	static Vector2 SMinus(const Vector2& v1, T minus)				{ return Vector2(v1.X - minus, v1.Y - minus); }
	static Vector2 SMultiply(const Vector2& v1, T scale)			{ return Vector2(v1.X * scale, v1.Y * scale); }
	static Vector2 SMultiply(const Vector2& v1, const Vector2& v2)	{ return Vector2(v1.X * v1.X, v1.Y * v1.Y); }
	static Vector2 SDivide(const Vector2& v1, T divid)				{ return Vector2(v1.X / divid, v1.Y / divid); }
	static Vector2 SDivide(const Vector2& v1, const Vector2& v2)	{ return Vector2(v1.X / v2.X, v1.Y / v2.Y); }

	/*
	Vector2f Vector2f::operator- () const					{ return Vector2f(-m_x, -m_y); }
	Vector2f Vector2f::operator+(const Vector2f &v) const	{ return Vector2f(m_x + v.m_x, m_y + v.m_y); }
	Vector2f Vector2f::operator-(const Vector2f &v) const	{ return Vector2f(m_x - v.m_x, m_y - v.m_y); }
	Vector2f Vector2f::operator*(float scale) const			{ return Vector2f(m_x * scale, m_y * scale); }
	Vector2f Vector2f::operator/(float divid) const			{ return Vector2f(m_x / divid, m_y / divid); }

	Vector2f& Vector2f::operator+=(const Vector2f &v)	{ Add(v);			return *this; }
	Vector2f& Vector2f::operator-=(const Vector2f &v)	{ Subtract(v);		return *this; }
	Vector2f& Vector2f::operator*=(float scale)			{ Scale(scale);		return *this; }
	Vector2f& Vector2f::operator*=(const Vector2f &v)	{ Scale(v);			return *this; }
	Vector2f& Vector2f::operator/=(float divid)			{ Divide(divid);	return *this; }
	Vector2f& Vector2f::operator/=(const Vector2f &v)	{ Divide(v);		return *this; }
	*/

public:
	static Vector2 s_down;		//Shorthand for writing Vector2f(0, -1).
	static Vector2 s_left;		//Shorthand for writing Vector2f(-1, 0).
	static Vector2 s_one;		//Shorthand for writing Vector2f(1, 1).
	static Vector2 s_right;	//Shorthand for writing Vector2f(1, 0).
	static Vector2 s_up;		//Shorthand for writing Vector2f(0, 1).
	static Vector2 s_zero;		//Shorthand for writing Vector2f(0, 0).



};


typedef Vector2<float> Vector2f;
typedef Vector2<int> Vector2i;



Vector2i Vector2i::s_down	= Vector2i( 0, -1);
Vector2i Vector2i::s_left	= Vector2i(-1,  0);
Vector2i Vector2i::s_one	= Vector2i( 1,  1);
Vector2i Vector2i::s_right	= Vector2i( 1,  0);
Vector2i Vector2i::s_up		= Vector2i( 0,  1);
Vector2i Vector2i::s_zero	= Vector2i( 0,  0);

Vector2f Vector2f::s_down	= Vector2f( 0.0f, -1.0f);
Vector2f Vector2f::s_left	= Vector2f(-1.0f,  0.0f);
Vector2f Vector2f::s_one	= Vector2f( 1.0f,  1.0f);
Vector2f Vector2f::s_right	= Vector2f( 1.0f,  0.0f);
Vector2f Vector2f::s_up		= Vector2f( 0.0f,  1.0f);
Vector2f Vector2f::s_zero	= Vector2f( 0.0f,  0.0f);
