#include "LogManager.h"
#include <stdarg.h>

LogManager gLogManager;





void LogManager::Register(BaseLogger* logger)
{
	m_loggers.push_back(logger);
}

void LogManager::Unregister(BaseLogger* logger)
{
	m_loggers.remove(logger);
}

void LogManager::Log(const LogVerbosity::Enum verbosity, const CallInfo callInfo, LogChannel channel, const char* format, ...)
{
	va_list argList;
	va_start(argList, format);
	for (auto it = m_loggers.begin(); it != m_loggers.end(); ++it)
	{
		(*it)->Log(verbosity, callInfo, channel, format, argList);
	}
	va_end(argList);
}
