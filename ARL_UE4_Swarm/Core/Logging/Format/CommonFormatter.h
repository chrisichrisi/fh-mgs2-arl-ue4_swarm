/********************************************************************
created:	2014/04/19
created:	19:4:2014   18:14
filename: 	Core\Logging\Format\CommonFormatter.h
file path:	Core\Logging\Format
file base:	CommonFormatter
file ext:	h
author:		Christoph Kappler

purpose:	Common formatters
*********************************************************************/
#pragma once

//************************************
// Includes
//************************************
#include "../Log.h"
#include <stdio.h>
#include <string.h>

#define _CRT_SECURE_NO_WARNINGS

//************************************
// Struct NoFormatter
// Purpose: Formats the log to "MESSAGE"
//************************************
struct NoFormatter
{
	void Format(FixedLogCharBuffer& buffer, const LogVerbosity::Enum verbosity, const CallInfo& callInfo, const LogChannel& channel, const char* format, va_list argList) const
	{
		int writtenChars = vsnprintf(buffer.m_buffer, buffer.m_size, format, argList);
		buffer.m_buffer[writtenChars] = '\n';
		buffer.m_buffer[writtenChars + 1] = '\0';
	}
};


//************************************
// Struct SimpleFormatter
// Purpose:	Formats the log to "VERBOSITY: MESSAGE"
//************************************
struct SimpleFormatter
{
	void Format(FixedLogCharBuffer& buffer, const LogVerbosity::Enum verbosity, const CallInfo& callInfo, const LogChannel& channel, const char* format, va_list argList) const
	{
		int currentBufferPos = 0;
		int remainingSpace = buffer.m_size;
		int result = sprintf(buffer.m_buffer, "%s: ", LogVerbosity::VerbosityToString(verbosity));
		remainingSpace -= result;
		currentBufferPos += result;
		//result = vsnprintf_s(buffer.m_buffer + currentBufferPos, remainingSpace, format, argList);
		result = vsnprintf(buffer.m_buffer + currentBufferPos, remainingSpace, format, argList);
		remainingSpace -= result;
		currentBufferPos += result;
		// TODO: Maybe add some checks here

		// set null terminator
		buffer.m_buffer[currentBufferPos] = '\n';
		buffer.m_buffer[currentBufferPos + 1] = '\0';
	}
};


//************************************
// Struct DebugFormatter:
// Purpose:	Formats the log to "FILENAME(LINENUMBER): [CHANNELNAME] (VERBOSITY) MESSAGE"
//************************************
struct DebugFormatter
{
	void Format(FixedLogCharBuffer& buffer, const LogVerbosity::Enum verbosity, const CallInfo& callInfo, const LogChannel& channel, const char* format, va_list argList) const
	{
		int currentBufferPos = 0;
		int remainingSpace = buffer.m_size;
		int result = sprintf(buffer.m_buffer, "%s(%i): [CHANNELNAME] (%s): ", callInfo.m_file, callInfo.m_lineNumber, LogVerbosity::VerbosityToString(verbosity));
		remainingSpace -= result;
		currentBufferPos += result;
		//result = vsnprintf_s(buffer.m_buffer + currentBufferPos, remainingSpace, format, argList);
		result = vsnprintf(buffer.m_buffer + currentBufferPos, remainingSpace, format, argList);
		remainingSpace -= result;
		currentBufferPos += result;
		// TODO: Maybe add some checks here

		// set null terminator
		buffer.m_buffer[currentBufferPos] = '\n';
		buffer.m_buffer[currentBufferPos + 1] = '\0';
	}
};

