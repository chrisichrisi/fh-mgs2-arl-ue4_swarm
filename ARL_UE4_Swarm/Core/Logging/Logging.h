/********************************************************************
created:	2014/04/19
created:	19:4:2014   18:19
filename: 	Core\Logging\Logging.h
file path:	Core\Logging
file base:	Logging
file ext:	h
author:		Christoph Kappler

purpose:	Interface for using Logging facilities
*********************************************************************/
#pragma once


//************************************
// Includes
//************************************
#include "Log.h"
#include "Logger.h"

#include "Filter/CommonFilters.h"
#include "Format/CommonFormatter.h"
#include "Output/CommonOutputter.h"
#include "Output/IDEOutput/IDEOutputter.h"


//************************************
// Typedefs
//************************************
typedef PrintFOutputter ConsoleOutputter;


typedef Logger<IDEOutputter, NoFormatter, NoFilter> PlainIDELogger;
typedef Logger<ConsoleOutputter, NoFormatter, NoFilter> PlainConsoleLogger;

typedef Logger<IDEOutputter, SimpleFormatter, NoFilter> SimpleIDELogger;
typedef Logger<ConsoleOutputter, SimpleFormatter, NoFilter> SimpleConsoleLogger;

typedef Logger<IDEOutputter, DebugFormatter, NoFilter> DebugIDELogger;
typedef Logger<ConsoleOutputter, DebugFormatter, NoFilter> DebugConsoleLogger;


typedef Logger<IDEOutputter, NoFormatter, VerbosityFilter> PlainVerbosityIDELogger;
typedef Logger<ConsoleOutputter, NoFormatter, VerbosityFilter> PlainVerbosityConsoleLogger;

typedef Logger<IDEOutputter, SimpleFormatter, VerbosityFilter> SimpleVerbosityIDELogger;
typedef Logger<ConsoleOutputter, SimpleFormatter, VerbosityFilter> SimpleVerbosityConsoleLogger;

typedef Logger<IDEOutputter, DebugFormatter, VerbosityFilter> DebugVerbosityIDELogger;
typedef Logger<ConsoleOutputter, DebugFormatter, VerbosityFilter> DebugVerbosityConsoleLogger;

