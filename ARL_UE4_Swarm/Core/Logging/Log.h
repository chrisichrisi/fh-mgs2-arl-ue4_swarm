/********************************************************************
created:	2014/04/19
created:	19:4:2014   18:08
filename: 	Core\Logging\Log.h
file path:	Core\Logging
file base:	Log
file ext:	h
author:		Christoph Kappler

purpose:	Different defines and other things used by the logging facilities
*********************************************************************/
#pragma once

//************************************
// Includes
//************************************
#include "..\typedefs.h"
#include "LogVerbosity.h"
#include "CallInfo.h"
#include "..\Memory\FixedBuffer.h"


//************************************
// Defines
//************************************
#define LOG_MAX_BUFFER_SIZE 512
#ifdef DEBUG_MODE
#define LOGGING_ENABLED
#endif


//************************************
// Typedefs
//************************************
typedef UINT32 LogChannel;
typedef FixedBuffer<char, LOG_MAX_BUFFER_SIZE>FixedLogCharBuffer;
