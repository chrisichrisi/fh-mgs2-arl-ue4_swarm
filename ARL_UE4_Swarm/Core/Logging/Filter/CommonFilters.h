/********************************************************************
created:	2014/04/19
created:	19:4:2014   18:11
filename: 	Core\Logging\Filter\CommonFilters.h
file path:	Core\Logging\Filter
file base:	CommonFilters
file ext:	h
author:		Christoph Kappler

purpose:	Common logging filters
*********************************************************************/
#pragma once

//************************************
// Includes
//************************************
#include "../Log.h"


//************************************
// Struct NoFilter
// Purpose: Does no filtering
//************************************
struct NoFilter
{
	bool PassedFilter(const LogVerbosity::Enum verbosity, const LogChannel channel)
	{
		return true;
	}
};


//************************************
// Struct VerbosityFilter
// Purpose:	Does filtering depending on verbosity level
//************************************
struct VerbosityFilter
{
	LogVerbosity::Enum m_verbosity;

	explicit VerbosityFilter(LogVerbosity::Enum verbosity) :
		m_verbosity(verbosity)
	{
	}

	bool PassedFilter(const LogVerbosity::Enum verbosity, const LogChannel channel)
	{
		return verbosity >= m_verbosity;
	}
};


//************************************
// Struct ChannelFilter
// Purpose: Does filtering depending on channel
//************************************
struct ChannelFilter
{
	bool PassedFilter(const LogVerbosity::Enum verbosity, const LogChannel channel)
	{
		// TODO: Implement
		return true;
	}
};
