/********************************************************************
created:	2014/04/19
created:	19:4:2014   18:21
filename: 	Core\Logging\Output\CommonOutputter.h
file path:	Core\Logging\Output
file base:	CommonOutputter
file ext:	h
author:		Christoph Kappler

purpose:	Some common outputter
*********************************************************************/
#pragma once

//************************************
// Includes
//************************************
#include <stdio.h>
#include <iostream>

#include "../Log.h"


//************************************
// Struct PrintFOutputter
// Purpose:	Outputs the buffer via printf
//************************************
struct PrintFOutputter
{
	void Output(const FixedLogCharBuffer& buffer)
	{
		printf("%s", buffer.m_buffer);
	}
};

//************************************
// Struct COUTOutputter
// Purpose: Outputs the buffer via std::cout
//************************************
struct COUTOutputter
{
	void Output(const FixedLogCharBuffer& buffer)
	{
		std::cout << buffer.m_buffer;
	}
};
