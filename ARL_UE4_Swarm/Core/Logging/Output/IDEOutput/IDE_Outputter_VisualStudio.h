#pragma once

#include <windows.h> // for OutputDebugStringA

struct IDEOutputter
{
	void Output(const FixedLogCharBuffer& buffer)
	{
		OutputDebugStringA(buffer.m_buffer);
	}
};

