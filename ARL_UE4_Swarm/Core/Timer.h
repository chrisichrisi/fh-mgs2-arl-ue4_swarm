#pragma once

#include "include.h"

struct Timer
{
private:
	double timeLastDT = 0.0f;
	double timeDT = 0.0f;
	double timeAbsLastFrame = 0.0f;
	double timeAbs = 0.0f;

public:
	float DT;

public:
	void ReadTime()
	{
		timeAbsLastFrame = timeAbs;
		timeLastDT = timeDT;
		timeAbs = glfwGetTime();
		timeDT = timeAbs - timeAbsLastFrame;

		DT = static_cast<float>(timeDT);
	}
};

