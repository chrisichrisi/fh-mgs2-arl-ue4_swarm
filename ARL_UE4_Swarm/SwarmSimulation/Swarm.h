#pragma once

#include "..\Core\typedefs.h"
#include "..\Core\Random.h"
#include "Particle.h"
#include "..\Core\Logging\LogVerbosity.h"
#include "..\Input\Input.h"
#include "..\Core\Logging\Logger.h"

#define _USE_MATH_DEFINES
#include <math.h>



#define RANDOM_ROTATION (M_PI / 8.0)
//#define RANDOM_ROTATION (M_PI / 2.0)



template <UINT32 PARTICLE_COUNT>
class Swarm
{
public:
	Particle m_particles[PARTICLE_COUNT];
	UINT32 m_particleCount = PARTICLE_COUNT;
	
	UINT32 m_leaderIndex = 0;

	Vector2f m_fieldMin;
	Vector2f m_fieldMax;

	Vector2i m_fieldDimensions;


	float m_randomRotation;

public:
	Swarm(Vector2i fieldDimensions) : 
		m_leaderIndex(0),
		m_fieldDimensions(fieldDimensions),
		m_fieldMin(Vector2f(0.0f, 0.0f)),
		m_fieldMax(Vector2f(static_cast<float>(fieldDimensions.X), static_cast<float>(fieldDimensions.Y))),
		m_randomRotation(static_cast<float>(RANDOM_ROTATION))
	{
		// Initialize particles
		for (UINT32 i = 0; i < m_particleCount; ++i)
		{
			float posX = Randomizer.GetFloat(static_cast<float>(m_fieldMin.X), static_cast<float>(m_fieldMax.X));
			float posY = Randomizer.GetFloat(static_cast<float>(m_fieldMin.Y), static_cast<float>(m_fieldMax.Y));

			m_particles[i].m_pos = Vector2f(posX, posY);

			m_particles[i].m_inertia = Randomizer.GetFloat(0.1f, 1.0f);
			m_particles[i].m_cohesion = Randomizer.GetFloat(0.1f, 1.0f);
			m_particles[i].m_separation = Randomizer.GetFloat(0.1f, 1.0f);
			m_particles[i].m_followshipLocal = Randomizer.GetFloat(0.1f, 1.0f);
			m_particles[i].m_followshipGlobal = Randomizer.GetFloat(0.1f, 1.0f);
			m_particles[i].m_backToBestPos = Randomizer.GetFloat(0.1f, 1.0f);
		}
	}


	void Update(float dt)
	{
		UpdateSwarm(dt);
	}

	void UpdateSwarm(float dt)
	{
		float moveSpeedPerSecond = 100.0f;
		float move = moveSpeedPerSecond * dt;


		Vector2f leaderPos = m_particles[m_leaderIndex].m_pos;

		MouseState mouseState = Input::GetMouseState();
		float inversedY = static_cast<float>(m_fieldMax.Y - mouseState.m_position.Y);
		Vector2f mousePos = Vector2f(static_cast<float>(mouseState.m_position.X), inversedY);
		Vector2f swarmMin;
		Vector2f swarmMax;

		float bestDistance = m_fieldMax.SqrMagnitude();
		int nextLeaderIndex = m_leaderIndex;



		
		Vector2f velos[PARTICLE_COUNT];

		float neighbourDistance = 30.0f;
		for (UINT32 i = 0; i < m_particleCount; ++i)
		{
			Vector2f pos = m_particles[i].m_pos;
			Vector2f velo;
			UINT32 neighbourCount = 0;
			UINT32 nearNeighbours = 0;

			int indexBestNeighbour = -1;
			float currentBestDistance = 0;
			// =============== Init local velocities ===============
			Vector2f veloA, veloB, veloC, veloD, veloE, veloF;

			// =============== GET LOCAL NEIGHBOURS ===============
			std::list<int> neighbourIndices;
			for (UINT32 j = 0; j < m_particleCount; ++j)
			{
				if (i == j) { continue; }
				float curDistance = Vector2f::Distance(pos, m_particles[j].m_pos);
				if (curDistance <= neighbourDistance)
				{
					neighbourIndices.push_back(j);
					++neighbourCount;

					// =============== CALCULATE: Move in center of mass ===============
					veloB.Add(m_particles[j].m_pos);
					// =============== CALCULATE: Separate from neighbours (when to close) ===============
					Vector2f neighbourDistanceVec = Vector2f::SMinus(pos, m_particles[j].m_pos);
					if (neighbourDistanceVec.SqrMagnitude() < Particle::s_collisionRadiusSquared)
					{
						veloC.Add(Vector2f::SMinus(m_particles[j].m_pos, pos));
						++nearNeighbours;
					}
					// =============== CALCULATE: Move towards best neighbour ===============
					if (indexBestNeighbour == -1 || (m_particles[j].m_distance < currentBestDistance))
					{
						indexBestNeighbour = j;
						currentBestDistance = m_particles[j].m_distance;
					}
				}
			}
			// =============== CALCULATE: Move in current direction ===============
			veloA = m_particles[i].m_velo;
			veloA.Scale(m_particles[i].m_inertia);
			// =============== CALCULATE: Move in center of mass ===============
			if (neighbourCount != 0)
			{
				veloB.Divide(static_cast<float>(neighbourCount));
				veloB.Subtract(pos);
			}
			
			veloB.Scale(m_particles[i].m_cohesion);
			// Rotate by random value
			veloB.RotateByRAD(Randomizer.GetFloat(-m_randomRotation, m_randomRotation));
			//GED_LOG(LogVerbosity::Info, 6, "%i: veloB(%f, %f)", i, veloB.X, veloB.Y);
			//veloB = Vector2f(0.0f, 0.0f);
			// =============== CALCULATE: Separate from neighbours (when to close) ===============
			if (nearNeighbours != 0)
			{
				veloC.Divide(static_cast<float>(nearNeighbours));
			}
			//veloC.Scale(-m_particles[i].m_separation);
			veloC.Scale(-1.0f);
			//GED_LOG(LogVerbosity::Info, 6, "%i: veloC(%f, %f)", i, veloC.X, veloC.Y);
			// Rotate by random value
			veloC.RotateByRAD(Randomizer.GetFloat(-m_randomRotation, m_randomRotation));
			// =============== CALCULATE: Move towards best neighbour ===============
			veloD = Vector2f::SMinus(m_particles[indexBestNeighbour].m_pos, pos);
			veloD.Scale(m_particles[i].m_followshipLocal);
			// Rotate by random value
			veloD.RotateByRAD(Randomizer.GetFloat(-m_randomRotation, m_randomRotation));
			// =============== CALCULATE: Move towards leader ===============
			Vector2f towards = (i == m_leaderIndex) ? mousePos : m_particles[m_leaderIndex].m_pos;
			veloE = Vector2f::SMinus(towards, pos);
			veloE.Scale(m_particles[i].m_followshipGlobal);
			// Rotate by random value
			veloE.RotateByRAD(Randomizer.GetFloat(-m_randomRotation, m_randomRotation));
			// =============== CALCULATE: Move towards best previous position ===============
			veloF = Vector2f::SMinus(m_particles[i].m_bestPosition, pos);
			veloF.Scale(m_particles[i].m_backToBestPos);
			veloF.RotateByRAD(Randomizer.GetFloat(-m_randomRotation, m_randomRotation));
			// =============== COMBINE ===============
			velo.Add(veloA).Add(veloB).Add(veloC).Add(veloD).Add(veloE).Add(veloF);
			velos[i] = velo;
		}

		// =============== Apply velocites ===============
		for (UINT32 i = 0; i < m_particleCount; ++i)
		{
			//m_particles[i].m_velo = velos[i].Scale(move);
			//m_particles[i].m_pos.Add(m_particles[i].m_velo);

			m_particles[i].m_velo = velos[i];
			m_particles[i].m_pos.Add(velos[i].Normalized().Scale(move));
			
			// Set to be in the boundaries
			if (m_particles[i].m_pos.X < m_fieldMin.X) { m_particles[i].m_pos.X = m_fieldMin.X; }
			if (m_particles[i].m_pos.Y < m_fieldMin.Y) { m_particles[i].m_pos.Y = m_fieldMin.Y; }
			if (m_particles[i].m_pos.X > m_fieldMax.X) { m_particles[i].m_pos.X = m_fieldMax.X; }
			if (m_particles[i].m_pos.Y > m_fieldMax.Y) { m_particles[i].m_pos.Y = m_fieldMax.Y; }

			// Calculate distance
			Vector2f distanceVec = Vector2f::SMinus(m_particles[i].m_pos, mousePos);
			m_particles[i].m_distance = distanceVec.Magnitude();
			if (m_particles[i].m_distance < m_particles[nextLeaderIndex].m_distance)
			{
				nextLeaderIndex = i;
			}

			Vector2f distanceBestPos = Vector2f::SMinus(m_particles[i].m_bestPosition, mousePos);
			if (m_particles[i].m_distance < distanceBestPos.Magnitude())
			{
				m_particles[i].m_bestPosition = m_particles[i].m_pos;
			}
		}
		m_leaderIndex = nextLeaderIndex;







		//m_leaderIndex = nextLeaderIndex;
		////m_swarmMiddle = (swarmMin + swarmMax) / 2.0f;
		//m_swarmMiddle = Vector2f::SDivide(Vector2f::SPlus(swarmMin, swarmMax), 2.0f);
		////GED_LOG(LogVerbosity::Info, 6, "Middle at (%f, %f)", m_swarmMiddle.X, m_swarmMiddle.Y);

	}


};

