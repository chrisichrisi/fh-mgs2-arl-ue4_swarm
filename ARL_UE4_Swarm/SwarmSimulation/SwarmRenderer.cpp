#include "SwarmRenderer.h"
#include "..\libs\glew-1.10.0\include\GL\glew.h"

void SwarmRenderer::Render(const SimSwarm& swarm)
{
	for (UINT32 i = 0; i < swarm.m_particleCount; ++i)
	{
		Vector2f fpos = swarm.m_particles[i].m_pos;
		GLvoid* colors;
		if (i == swarm.m_leaderIndex)
		{
			colors = m_leaderParticleCol;
		}
		else
		{
			colors = m_particleCol;
		}
		glRasterPos2f(fpos.X, fpos.Y);
		glDrawPixels(1, 1, GL_RGB, GL_UNSIGNED_BYTE, colors);
	}
}
