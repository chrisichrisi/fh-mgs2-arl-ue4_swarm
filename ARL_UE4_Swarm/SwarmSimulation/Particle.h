#pragma once
#include "..\Core\Math\Vector2.h"
struct Particle
{
	Vector2f m_pos;
	Vector2f m_velo;


	float m_inertia;
	float m_cohesion;
	float m_separation;
	float m_followshipLocal;
	float m_followshipGlobal;
	float m_backToBestPos;



	float m_distance;

	Vector2f m_bestPosition;

	
	static float s_collisionRadiusSquared;
};

