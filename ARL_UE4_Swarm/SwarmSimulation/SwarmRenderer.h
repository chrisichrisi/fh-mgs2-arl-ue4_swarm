#pragma once

#include "SwarmParams.h"

class SwarmRenderer
{
private:
	unsigned char m_particleCol[3];
	unsigned char m_leaderParticleCol[3];
public:
	SwarmRenderer()
	{
		m_particleCol[0] = 255; 
		m_particleCol[1] = 255; 
		m_particleCol[2] = 255;

		m_leaderParticleCol[0] = 255;
		m_leaderParticleCol[1] = 0;
		m_leaderParticleCol[2] = 0;
	}
	
	void Render(const SimSwarm& swarm);


};

