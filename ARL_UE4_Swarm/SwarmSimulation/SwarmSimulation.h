#pragma once


//#include "SwarmParams.h"
#include "SwarmRenderer.h"




class SwarmSimulation
{
public:
	SimSwarm m_swarm;
	SwarmRenderer m_renderer;



public:
	SwarmSimulation();


	void Start()	{ }
	void Stop()		{ }

	void Update(float dt)	
	{
		m_swarm.Update(dt); 
	}
	void Render()
	{ 
		m_renderer.Render(m_swarm); 
	}
	






	Vector2i GetWindowSize() const { return m_swarm.m_fieldDimensions; }
	const char* GetWindowTitle() const { return "Swarm Simulation"; }
};

