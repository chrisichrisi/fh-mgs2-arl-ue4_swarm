#pragma once

#include "Swarm.h"


#define WIDTH 512
#define HEIGHT 512

#define SWARM_PARTICLE_COUNT 100
//#define SWARM_PARTICLE_COUNT 20
//#define SWARM_PARTICLE_COUNT 5

#define COLLISION_RADIUS 50.0f
//#define COLLISION_RADIUS 25.0f
//#define COLLISION_RADIUS 10.0f




typedef Swarm<SWARM_PARTICLE_COUNT> SimSwarm;


