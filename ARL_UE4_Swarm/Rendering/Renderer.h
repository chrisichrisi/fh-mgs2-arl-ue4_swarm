#pragma once

#include "../IncludeGLFW.h"



#include "../Core/Logging/Logger.h"
#include "../Core/Math/Vector2.h"



class Renderer
{
private:
	GLFWwindow* m_window;
	Vector2i m_windowSize;


public:
	Renderer();
	~Renderer();

	template<class SIMULATOR>
	void Render(SIMULATOR& simulator)
	{
		BeginScene();
		RenderScene(simulator);
		EndScene();
	}

	void BeginScene()
	{
		glClear(GL_COLOR_BUFFER_BIT);
		glLoadIdentity();
	}

	template<class SIMULATOR>
	void RenderScene(SIMULATOR& simulator)
	{
		simulator.Render();
	}
	void EndScene()
	{
		glfwSwapBuffers(m_window);
	}



	void Resize(Vector2i windowSize)
	{
		m_windowSize = windowSize;

		//glViewport(0, 0, width, height);
		glViewport(0, 0, windowSize.X, windowSize.Y);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		//glOrtho(0.0, width, 0.0, height, -1.0, 1.0);
		glOrtho(0.0, windowSize.X, 0.0, windowSize.Y, -1.0, 1.0);

		glMatrixMode(GL_MODELVIEW);
	}



	void Init(GLFWwindow* window, Vector2i windowSize)
	{
		if (GLEW_OK != glewInit())
		{
			GED_LOG(LogVerbosity::Fatal, 3, "GLEW failed to initialize");
		}

		m_window = window;

		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glDisable(GL_DEPTH_TEST);
		glDisable(GL_LIGHTING);

		Resize(windowSize);






		// Check for error
		if (glGetError() != GL_NO_ERROR)
		{
			GED_LOG(LogVerbosity::Fatal, 3, "Some OpenGL-Error occured");
		}
	}
};

