
#include "Core/defines.h"

#include "Core/Logging/Logging.h"
#include "Simulation.h"
#include "SwarmSimulation/SwarmSimulation.h"





int main(int argc, const char** argv)
{
	
	GED_NEW_LOGGER(ideLogger, DebugIDELogger);
	GED_NEW_LOGGER(consoleLogger, SimpleConsoleLogger);

	Simulation<SwarmSimulation> sim;

	GED_LOG(LogVerbosity::Info, 0, "Initializing Simulation.");
	if (sim.Init())
	{
		GED_LOG(LogVerbosity::Info, 0, "Starting Simulation.");
		sim.Run();
	}	
	GED_LOG(LogVerbosity::Info, 0, "Terminating Simulation.");


	return 0;
}


