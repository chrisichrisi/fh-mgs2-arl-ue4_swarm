#pragma once
#include "MouseState.h"
#include "../IncludeGLFW.h"





class Input
{
private:
	GLFWwindow* m_window;

	MouseState m_oldMouseState;
	MouseState m_mouseState;



public:
	static MouseState& GetMouseState()
	{
		return s_instance.m_mouseState;
	}
	
	static void Init(GLFWwindow* window) { s_instance.InitInternal(window); }

	static void Update()
	{
		s_instance.UpdateInternal();
	}


	static bool IsKeyDown(int key)
	{
		return s_instance.IsKeyDownInternal(key);
	}

private:
	void UpdateInternal()
	{
		double mouseX, mouseY;
		glfwGetCursorPos(m_window, &mouseX, &mouseY);



		m_oldMouseState = m_mouseState;
		m_mouseState.m_position = Vector2i((int)mouseX, (int)mouseY);
	}

	bool IsKeyDownInternal(int key)
	{
		return (glfwGetKey(m_window, key) == GLFW_PRESS);
	}



	Input() { }
	void InitInternal(GLFWwindow* window) { m_window = window; }

	static Input s_instance;
};

