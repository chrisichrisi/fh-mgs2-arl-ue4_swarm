#pragma once


#include "Rendering\Renderer.h"
#include "Input\Input.h"



template <class SIMULATOR>
class Simulation
{
private:
	SIMULATOR m_simulator;
	Renderer m_renderer;

	Vector2i m_windowSize;

	GLFWwindow* m_window;
	std::string m_title;
	bool m_exit = false;

	double timeLastDT = 0.0f;
	double timeDT = 0.0f;
	double timeAbsLastFrame = 0.0f;
	double timeAbs = 0.0f;

public:
	
	bool Init()
	{
		m_windowSize = m_simulator.GetWindowSize();
		const char* windowTitle = m_simulator.GetWindowTitle();



		m_title = std::string(windowTitle);

		if (InitInternal() != true) { return false; }

		// Initialize
		Input::Init(m_window);
		m_renderer.Init(m_window, m_windowSize);
		//m_simulator.Init(_window);

		return true;
	}

	void Run()
	{
		Start();
		while (!m_exit)
		{
			ReadTime();
			Update(static_cast<float>(timeDT));
			Render();
			// Poll for and process events
			glfwPollEvents();
			m_exit = Input::IsKeyDown(GLFW_KEY_ESCAPE);
		}
		Stop();
	}



private:
	void Start()
	{
		m_simulator.Start();
	}
	void Stop()
	{
		m_simulator.Stop();
		glfwTerminate();
	}
	void Update(float dt)
	{
		Input::Update();
		m_simulator.Update(dt);
	}
	void Render()
	{
		m_renderer.Render(m_simulator);
	}

	void ReadTime()
	{
		timeAbsLastFrame = timeAbs;
		timeLastDT = timeDT;
		timeAbs = glfwGetTime();
		timeDT = timeAbs - timeAbsLastFrame;
	}




	bool InitInternal()
	{
		// Initialize the library
		if (!glfwInit()) 
		{ 
			GED_LOG(LogVerbosity::Fatal, 1, "Could not initialize GLFW");
			return false; 
		}

		// Window will not be resizeable
		glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

		// Create a windowed mode window and its OpenGL context
		m_window = glfwCreateWindow(m_windowSize.X, m_windowSize.Y, m_title.c_str(), NULL, NULL);
		if (!m_window)
		{
			GED_LOG(LogVerbosity::Fatal, 1, "Could not create GLFW-Window");
			glfwTerminate();
			return false;
		}

		// Make the window's context current
		glfwMakeContextCurrent(m_window);

		// Disable VSync
		//glfwSwapInterval(0);

		return true;
	}
};

